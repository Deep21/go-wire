//+build wireinject

package main

import (
	"github.com/google/wire"
)

var Set = wire.NewSet(
	NewStructRR,
	wire.Bind(new(I1), new(RR)),
	NewStructT)

func initializeT() T {
	wire.Build(wire.NewSet(
		NewStructRR,
		wire.Bind(new(I1), new(RR)),
		NewStructT))
	return T{}
}
