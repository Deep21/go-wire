package main

import "fmt"

type I1 interface {
	M1()
}

type I2 interface {
	M2()
}

type T struct {
	i1 I1
}

type R struct{}

func (R) M1() {
	fmt.Println("M1")
}

type RR struct{}

func (RR) M1() {
	fmt.Println("M1 from RR")

}

// func f1(i I1) {
// 	i.M1()
// 	xType := fmt.Sprintf("%T", i)
// 	fmt.Println(xType)
// }

// func f2(i I2) {
// 	i.M2()
// 	xType := fmt.Sprintf("%T", i)
// 	fmt.Println(xType)
// }

func NewStructR() R {
	return R{}
}

func NewStructRR() RR {
	return RR{}
}

func NewStructT(r I1) T {
	return T{i1: r}
}

func main() {
	initializeT().i1.M1()

}
